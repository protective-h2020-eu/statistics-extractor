FROM docker
MAINTAINER Antonio Perez <apbautista@gmv.com>

COPY statisticsExtractor /bin/statisticsExtractor

# Run the cron every minute
RUN echo '0  2  *  *  *  /bin/statisticsExtractor' > /var/spool/cron/crontabs/root

CMD crond -l 2 -f
